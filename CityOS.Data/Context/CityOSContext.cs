﻿using CityOS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CityOS.Data.Context
{
    public class CityOSContext: DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Picture> Pictures { get; set; }
        public DbSet<Wanted> Wanted { get; set; }
        public DbSet<IdentifiedPicture> IdentifiedPictures { get; set; }

        public CityOSContext(DbContextOptions<CityOSContext> options) : base(options) { }

        public CityOSContext(string connectionString) : this(GetOptions(connectionString))
        {
        }

        private static DbContextOptions<CityOSContext> GetOptions(string connectionString)
        {
            return (DbContextOptions<CityOSContext>)SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionString).Options;
        }

    }
}
