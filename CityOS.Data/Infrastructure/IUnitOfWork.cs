﻿using CityOS.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace CityOS.Data.Infrastructure
{
    public interface IUnitOfWork
    {
        void Dispose();
        void Save();
        void Dispose(bool disposing);
        Repository<T> Repository<T>() where T : BaseEntity;
    }
}
