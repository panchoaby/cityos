﻿using CityOS.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using Vroom.Common.Infrastructure;

namespace CityOS.Data.Infrastructure
{
    public class DatabaseFactory : IDatabaseFactory
    {
        private DbContext _dataContext;
        private string ConnectionString= "Server=tcp:ctos.database.windows.net,1433;Initial Catalog=CityOSDB;Persist Security Info=False;User ID=cityos-admin;Password=oceans.11;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";

        public DbContext Get()
        {
            return _dataContext ?? (_dataContext = new CityOSContext(ConnectionString));
        }

        public void Dispose()
        {
            if (_dataContext != null)
                _dataContext.Dispose();
        }
    }
}
