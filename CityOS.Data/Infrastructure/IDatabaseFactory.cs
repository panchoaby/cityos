﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CityOS.Data.Infrastructure
{
    public interface IDatabaseFactory
    {
        DbContext Get();
    }
}
