﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using CityOS.Data.Context;

namespace CityOS.Data.Migrations
{
    [DbContext(typeof(CityOSContext))]
    [Migration("20170620001039_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("CityOS.Domain.Entities.IdentifiedPicture", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal>("Confidance");

                    b.Property<DateTime>("InsertedDate");

                    b.Property<int?>("PictureId");

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("PictureId");

                    b.HasIndex("UserId");

                    b.ToTable("IdentifiedPictures");
                });

            modelBuilder.Entity("CityOS.Domain.Entities.Picture", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("InsertedDate");

                    b.Property<string>("Name");

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<int?>("UploaderId");

                    b.Property<int?>("WantedId");

                    b.HasKey("Id");

                    b.HasIndex("UploaderId");

                    b.HasIndex("WantedId");

                    b.ToTable("Pictures");
                });

            modelBuilder.Entity("CityOS.Domain.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<DateTime>("InsertedDate");

                    b.Property<string>("LastName");

                    b.Property<string>("Password");

                    b.Property<int>("Status");

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<string>("UserName");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("CityOS.Domain.Entities.Wanted", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<DateTime>("InsertedDate");

                    b.Property<string>("Name");

                    b.Property<int>("Status");

                    b.Property<DateTime>("UpdatedDate");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("Wanted");
                });

            modelBuilder.Entity("CityOS.Domain.Entities.IdentifiedPicture", b =>
                {
                    b.HasOne("CityOS.Domain.Entities.Picture", "Picture")
                        .WithMany("IdentifiedPictures")
                        .HasForeignKey("PictureId");

                    b.HasOne("CityOS.Domain.Entities.User", "User")
                        .WithMany("IdentifiedPictures")
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("CityOS.Domain.Entities.Picture", b =>
                {
                    b.HasOne("CityOS.Domain.Entities.User", "Uploader")
                        .WithMany()
                        .HasForeignKey("UploaderId");

                    b.HasOne("CityOS.Domain.Entities.Wanted", "Wanted")
                        .WithMany("Pictures")
                        .HasForeignKey("WantedId");
                });

            modelBuilder.Entity("CityOS.Domain.Entities.Wanted", b =>
                {
                    b.HasOne("CityOS.Domain.Entities.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });
        }
    }
}
