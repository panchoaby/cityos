﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CityOS.Domain.Infrastructure
{
    public class BaseEntity
    {
        public int Id { get; set; }
        public DateTime InsertedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
    }
}
