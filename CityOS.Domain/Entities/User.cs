﻿using CityOS.Domain.Infrastructure;
using System.Collections.Generic;

namespace CityOS.Domain.Entities
{
    public class User: BaseEntity
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public int Status { get; set; }

        public virtual ICollection<IdentifiedPicture> IdentifiedPictures { get; set; }
    }
}
