﻿using CityOS.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace CityOS.Domain.Entities
{
    public class Picture:BaseEntity
    {
        public string Name { get; set; }

        public virtual User Uploader { get; set; }
        public virtual ICollection<IdentifiedPicture> IdentifiedPictures { get; set; }
        public virtual Wanted Wanted { get; set; }
    }
}
