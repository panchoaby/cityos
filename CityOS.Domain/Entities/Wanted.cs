﻿using CityOS.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace CityOS.Domain.Entities
{
    [Table("Wanted")]
    public class Wanted: BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }

        public virtual User User { get; set; }
        public virtual ICollection<Picture> Pictures { get; set; }
    }
}
