﻿using CityOS.Domain.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace CityOS.Domain.Entities
{
    public class IdentifiedPicture:BaseEntity
    {
        public decimal Confidance { get; set; }

        public virtual User User { get; set; }
        public virtual Picture Picture { get; set; }
    }
}
