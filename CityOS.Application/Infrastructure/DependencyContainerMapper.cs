﻿using CityOS.Data.Context;
using CityOS.Data.Infrastructure;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace CityOS.Application.Infrastructure
{
    public class DependencyContainerMapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            //database  
            services.AddDbContext<CityOSContext>();
            //services.AddScoped<IDatabaseFactory, DatabaseFactory>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            //register generic repository
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            //register generic service
            services.AddScoped(typeof(IService<>), typeof(Service<>));

        }
    }
}
