﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CityOS.Domain.Entities;

namespace CityOS.Web.Infrastructure
{
    public class AccountRequirement : IAuthorizationRequirement { }

    public class AccountHandler : AuthorizationHandler<AccountRequirement>
    {

        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, AccountRequirement requirement)
        {
            var controllerContext = context.Resource as ActionContext;
            var user = controllerContext.HttpContext.Session.Get<User>("User");
            if (user!=null && user.Id>0)
            {
                context.Succeed(requirement);
                return;
            }
        }
    }
}
