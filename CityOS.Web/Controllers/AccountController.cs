using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CityOS.Application.Infrastructure;
using CityOS.Domain.Entities;
using CityOS.Web.Model.Account;
using Omu.ValueInjecter;
using static CityOS.Common.Infrastructure.Enums;

namespace CityOS.Web.Controllers
{
    public class AccountController : BaseController
    {
        public readonly IService<User> _userService;

        public AccountController(IService<User> userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View(new RegisterModel());
        }

        [HttpPost]
        public IActionResult Register(RegisterModel registerModel)
        {
            if(ModelState.IsValid)
            {
                var user = _userService.GetFirst(x => x.Email == registerModel.Email);
                if(user !=null) return RedirectToAction("Index", "Home");
                user = (User)new User().InjectFrom(registerModel);
                user.Status = (int)UserStatus.Active;
                var registeredUser = _userService.Add(user);
                Identity = registeredUser;
            }
            return RedirectToAction("Index","Home");
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View(new LoginModel());
        }

        [HttpPost]
        public IActionResult Login(LoginModel loginModel)
        {
            if (ModelState.IsValid)
            {
                var user = _userService.GetFirst(x => x.UserName == loginModel.UserName && x.Password == loginModel.Password);
                if (user == null) return RedirectToAction("Index", "Home");
                Identity = user;
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Logout()
        {
            Identity = null;
            return RedirectToAction("Index", "Home");
        }
    }
}