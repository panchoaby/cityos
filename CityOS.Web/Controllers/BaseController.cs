﻿using CityOS.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using CityOS.Web.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;

namespace CityOS.Web.Controllers
{
    public class BaseController : Controller
    {
        public User Identity
        {
            get
            {
                return HttpContext.Session.Get<User>("User");
            }
            set
            {
                HttpContext.Session.Set<User>("User", value);
            }
        }
    }

    public class BaseAuthController : BaseController
    {
        //public class AccountRequirement : IAuthorizationRequirement { }

        //public class AccountHandler : AuthorizationHandler<AccountRequirement>
        //{
        //    protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, AccountRequirement requirement)
        //    {
        //        if (context.HttpContext.Session != null)
        //            if (context.User.IsInRole("fooBar"))
        //            {
        //                context.Succeed(requirement);
        //                return;
        //            }
        //    }
        //}
    }
}
