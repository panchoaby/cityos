﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CityOS.Domain.Entities;
using CityOS.Application.Infrastructure;
using Microsoft.AspNetCore.Http;

namespace CityOS.Web.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(IService<User> userService)
        {

        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
