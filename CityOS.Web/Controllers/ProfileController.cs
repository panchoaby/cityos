using Microsoft.AspNetCore.Mvc;
using CityOS.Application.Infrastructure;
using CityOS.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using static CityOS.Common.Infrastructure.Enums;
using CityOS.Web.Model.Profile;
using Omu.ValueInjecter;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage;
using Vroom.Common.Infrastructure;
using Microsoft.WindowsAzure.Storage.Blob;
using System.IO;
using Microsoft.Net.Http.Headers;
using System.Configuration;
using Microsoft.Extensions.Configuration;

namespace CityOS.Web.Controllers
{
    [Authorize(Policy= nameof(Policy.Account))]
    public class ProfileController : BaseController
    {
        public readonly IService<User> _userService;
        public readonly IService<Picture> _pictureService;
        public readonly IService<IdentifiedPicture> _identifiedPictureService;
        public IConfiguration Configuration { get; set; }

        public ProfileController(IService<User> userService, IService<Picture> pictureService, IService<IdentifiedPicture> identifiedPictureService, IConfiguration configuration)
        {
            Configuration = configuration;
            _userService = userService;
            _pictureService = pictureService;
            _identifiedPictureService = identifiedPictureService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var user = new ProfileModel().InjectFrom(Identity);
            return View(user);
        }

        [HttpPost]
        public IActionResult Index(ProfileModel profileUser)
        {
            var dbUser = _userService.GetByID(Identity.Id);
            dbUser.InjectFrom(profileUser);
            _userService.Update(dbUser);
            Identity = dbUser;
            return View(new ProfileModel().InjectFrom(dbUser));
        }

        [HttpGet]
        public async Task<IActionResult> Pictures()
        {
            var userId = Identity.Id;
            var storageAccount = CloudStorageAccount.Parse(Configuration.GetConnectionString("AzureStorageAccountConnectionString"));
            var blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("users");

            var model = new List<string>();
            container.GetDirectoryReference("users");
            BlobContinuationToken token = null;
            do
            {
                BlobResultSegment resultSegment = await container.ListBlobsSegmentedAsync("1/", token);
                token = resultSegment.ContinuationToken;

                foreach (IListBlobItem item in resultSegment.Results)
                {
                    CloudBlockBlob blob = (CloudBlockBlob)item;
                    model.Add(blob.Uri.AbsoluteUri);
                }
            } while (token != null);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Pictures(List<IFormFile> files)
        {
            var userId = Identity.Id;
            var storageAccount = CloudStorageAccount.Parse(Configuration.GetConnectionString("AzureStorageAccountConnectionString"));
            var blobClient = storageAccount.CreateCloudBlobClient();
            CloudBlobContainer container = blobClient.GetContainerReference("users");
            if (await container.CreateIfNotExistsAsync())
            {
                await container.SetPermissionsAsync(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
            }

            var user = _userService.GetByID(Identity.Id);

            for (int i = 0; i < files.Count; i++)
            {
                var blockBlob = container.GetBlockBlobReference($"{userId}/{files[i].FileName}");
                if (await blockBlob.ExistsAsync())
                {
                    await blockBlob.DeleteAsync();
                }
                blockBlob.Properties.ContentType = files[i].ContentType;
                await blockBlob.UploadFromStreamAsync(files[i].OpenReadStream());
                var picture = new Picture { Uploader = user, Name = files[i].FileName };
                _pictureService.Add(picture);
                _identifiedPictureService.Add(new IdentifiedPicture { User = user, Picture = picture, Confidance = 1 });
            }
            return View();
        }
    }
}