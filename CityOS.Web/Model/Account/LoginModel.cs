﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CityOS.Web.Model.Account
{
    public class LoginModel
    {
        [Required(ErrorMessage = "UserName is required")]
        public string UserName { get; set; }
        [DataType(DataType.Password)]
        [StringLength(255, ErrorMessage = "Must be at least 4 characters long", MinimumLength = 4)]
        public string Password { get; set; }
    }
}
