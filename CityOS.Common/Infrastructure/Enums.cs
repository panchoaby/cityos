﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CityOS.Common.Infrastructure
{
    public static class Enums
    {
        public enum UserStatus
        {
            Inactive = 0,
            Active = 1,
            Deleted = 2
        }

        public enum Policy { Account };
    }
}
