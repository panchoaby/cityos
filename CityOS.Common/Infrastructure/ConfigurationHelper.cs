﻿using System.Configuration;

namespace Vroom.Common.Infrastructure
{
    public class ConfigurationHelper
    {
        public static string GetSettingAsString(string settingName)
        {
            return ConfigurationManager.AppSettings[settingName];
        }

        public static string GetConnectionAsString(string settingName)
        {
            return ConfigurationManager.ConnectionStrings[settingName].ConnectionString;
        }
    }
}
